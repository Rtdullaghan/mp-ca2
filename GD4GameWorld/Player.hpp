#pragma once
#include "Command.hpp"
#include "Clock.hpp"
#include "KeyBinding.hpp"

#include <SFML/System/NonCopyable.hpp>
#include "SFML/Window/Event.hpp"
#include <SFML/Network/TcpSocket.hpp>

#include <map>

class CommandQueue;

class Player
{
public:
public:
	typedef PlayerAction::Type Action;
	enum class MissionStatus{
		MissionRunning, 
		MissionSuccessP1,
		MissionSuccessP2,
		MissionFailure
	};

public:
	Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding);
	void handleEvent(const sf::Event& event, CommandQueue& commands);
	void handleRealtimeInput(CommandQueue& commands);
	void handleRealtimeNetworkInput(CommandQueue& commands);
	void handleNetworkEvent(Action action, CommandQueue& commands);
	void handleNetworkRealtimeChange(Action action, bool actionEnabled);
	bool isMarkedForRemoval();
	void setMarkedForRemoval();

	std::string getTime();
	void pauseTimer();
	void unpauseTimer();
	void resetTimer();

	sf::Int32 getIdentifier();
	void setMissionStatus(MissionStatus status);
	MissionStatus getMissionStatus() const;
	void disableAllRealtimeActions();
	bool isLocal() const;
	float mSpeed;
private:
	void initializeActions();


private:
	bool hasLost;

	const KeyBinding*			mKeyBinding;
	std::map<Action, bool>		mActionProxies;
	std::map<Action, Command> mActionBinding;
	MissionStatus mCurrentMissionStatus;
	Stopwatch mTimer;
	sf::Int32 mIdentifier;
	sf::TcpSocket*				mSocket;
	bool              mMarkedForRemoval;
};