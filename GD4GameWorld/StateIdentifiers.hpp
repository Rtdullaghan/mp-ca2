#pragma once
enum class StateIDs {
	None,
	Title,
	Menu,
	Game,
	Settings,
	Pause,
	NetworkPause,
	MissionSuccess,
	GameOver,
	LeaderBoard,
	HostGame,
	JoinGame
};