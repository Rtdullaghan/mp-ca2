#include "GameState.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include "MusicPlayer.hpp"

GameState::GameState(StateStack& stack, Context context) 
	:State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds)
	, mPlayer(nullptr, 1, context.keys1)	
{
	mPlayer.setMissionStatus(Player::MissionStatus::MissionRunning);
	mWorld.setMainPlayerIdentifier(1);
	mWorld.addCharacter(1);
	//Play the mission theme
	mPlayer.resetTimer();
	context.music->play(MusicIDs::MissionTheme);
	mWorld.startGame();
}

void GameState::draw()
{
	mWorld.draw();
}

bool GameState::update(sf::Time dt)
{
	mWorld.update(dt);

	if (mWorld.hasPlayerReachedEnd() != -1)
	{
		
		mWorld.logWinners();
	
		requestStackPush(StateIDs::LeaderBoard);
	}

	if (!mWorld.hasAlivePlayer())
	{
		mPlayer.setMissionStatus(Player::MissionStatus::MissionFailure);
		requestStackPush(StateIDs::GameOver);
	}

	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer.handleRealtimeInput(commands);

	return true;
}

bool GameState::handleEvent(const sf::Event & event)
{
	//Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer.handleEvent(event, commands);

	//Escape pressed, trigger pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
	{
		requestStackPush(StateIDs::Pause);
	}

	return true;
}


